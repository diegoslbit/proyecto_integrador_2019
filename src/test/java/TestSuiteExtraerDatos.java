package test.java;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ TestExtraccionPorMetodoMtp.class, TestExtraccionPorMetodoAdb.class,
	TestExtraccionPorMetodoDirtyCow.class, TestExtraccionPorMetodoRecovery.class, 
	TestExtraccionPorMetodoLgLaf.class })
public class TestSuiteExtraerDatos {}
