package test.java;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.junit.Before;
import org.junit.Test;

import main.java.model.DetectarManual;
import main.java.model.Dispositivo;
import main.java.model.ExtractorDatos;
import main.java.model.ExtraerPorRecovery;
import main.java.model.ReconocedorDispositivo;
import main.java.model.UnidadAlmacenamiento;

public class TestExtraccionPorMetodoRecovery 
{
	private static List<Dispositivo> listaDispositivos;
	private ReconocedorDispositivo reconocedorDispositivo;
	private UnidadAlmacenamiento unidadAlmacenamiento;
	private ExtractorDatos extractorDatos;
	private Scanner entrada;
	
	@Before
	public void init()
	{
		listaDispositivos = new ArrayList<Dispositivo>();
		listaDispositivos.add(new Dispositivo("samsung", "GT-I8550L", "4.1.2", "JZO54K.I8550LUBUANF1"));
		listaDispositivos.add(new Dispositivo("samsung", "GT-I8260L", "4.1.2", "JZO54K.I8260LUBAMG3"));
		listaDispositivos.add(new Dispositivo("LGE", "LG-D625", "4.4.2", "KOT49I"));
		listaDispositivos.add(new Dispositivo("LGE", "Nexus 4", "4.2.2", "JDQ39"));
		listaDispositivos.add(new Dispositivo("LGE", "LG-H440AR", "5.0.1", "LRX21Y"));
		listaDispositivos.add(new Dispositivo("LGE", "LG-M250", "7.0", "NRD90U"));
		listaDispositivos.add(new Dispositivo("motorola", "XT890", "4.1.2", "9.8.2I-50_SML-23"));		
	}
	
	@Test
	public void testExtraerPorRecovery() 
	{
		System.out.println("////////////////TEST EXTRAER POR METODO RECOVERY////////////////");
		entrada = new Scanner(System.in);
		
		reconocedorDispositivo = new DetectarManual(listaDispositivos, "samsung, GT-I8550L, 4.1.2, JZO54K.I8550LUBUANF1");
		reconocedorDispositivo.detectarDispositivo();
		System.out.print("Conectar dispositivo samsung, GT-I8550L, 4.1.2, JZO54K.I8550LUBUANF1 a ordenador. Pulsar ENTER para avanzar: ");
		entrada.nextLine();
		System.out.print("Introducir path directorio de unidad de almacenamiento: ");		
		String rutaDestino = entrada.nextLine(); // /home/diego/Documentos/Extraccion
		unidadAlmacenamiento = new UnidadAlmacenamiento();
		unidadAlmacenamiento.guardarRutaDestino(rutaDestino);
		
		extractorDatos = new ExtraerPorRecovery(reconocedorDispositivo.getDispositivoDetectado(), unidadAlmacenamiento, true, true);
		
		assertEquals(0, extractorDatos.getEstadoExtraccionDatos());
		System.out.print("Colocar dispositivo en modo bootloader. Pulsar ENTER para avanzar: ");		
		entrada.nextLine();
		extractorDatos.extraerDatos();
		assertEquals(1, extractorDatos.getEstadoExtraccionDatos());
		extractorDatos.extraerDatos();
		assertEquals(2, extractorDatos.getEstadoExtraccionDatos());
		extractorDatos.extraerDatos();
		assertEquals(3, extractorDatos.getEstadoExtraccionDatos());
		System.out.print("Colocar dispositivo en modo recovery. Pulsar ENTER para avanzar: ");
		entrada.nextLine();
		extractorDatos.extraerDatos();
		assertEquals(4, extractorDatos.getEstadoExtraccionDatos());
		extractorDatos.extraerDatos();
		assertEquals(5, extractorDatos.getEstadoExtraccionDatos());
		System.out.print("Colocar dispositivo en modo bootloader nuevamente. Pulsar ENTER para avanzar: ");
		entrada.nextLine();
		extractorDatos.extraerDatos();
		assertEquals(6, extractorDatos.getEstadoExtraccionDatos());
		extractorDatos.extraerDatos();
		assertEquals(7, extractorDatos.getEstadoExtraccionDatos());
		extractorDatos.extraerDatos();
		assertEquals(8, extractorDatos.getEstadoExtraccionDatos());
		extractorDatos.extraerDatos();
		assertEquals(9, extractorDatos.getEstadoExtraccionDatos());
		System.out.println("////////////////////////////////////////////////////////////////");
	}
	
	@Test
	public void testEspacioAlmacenamientoInsuficiente() 
	{	
		System.out.println("////////////////TEST  ESPACIO DE ALMACENAMIENTO INSUFICIENTE////////////////");
		entrada = new Scanner(System.in);
		
		reconocedorDispositivo = new DetectarManual(listaDispositivos, "samsung, GT-I8550L, 4.1.2, JZO54K.I8550LUBUANF1");
		reconocedorDispositivo.detectarDispositivo();
		System.out.print("Conectar dispositivo samsung, GT-I8550L, 4.1.2, JZO54K.I8550LUBUANF1 a ordenador. Pulsar ENTER para avanzar: ");
		entrada.nextLine();
		System.out.print("Introducir path directorio de unidad de almacenamiento: ");		
		String rutaDestino = entrada.nextLine(); // /media/diego/KINGSTON/Extraccion_de_datos
		unidadAlmacenamiento = new UnidadAlmacenamiento();
		unidadAlmacenamiento.guardarRutaDestino(rutaDestino);
		
		extractorDatos = new ExtraerPorRecovery(reconocedorDispositivo.getDispositivoDetectado(), unidadAlmacenamiento, true, true);
		
		assertEquals(0, extractorDatos.getEstadoExtraccionDatos());
		System.out.print("METODO RECOVERY: Colocar en modo bootloader a ordenador. Pulsar ENTER para avanzar: ");		
		entrada.nextLine();
		extractorDatos.extraerDatos();
		assertEquals(1, extractorDatos.getEstadoExtraccionDatos());
		extractorDatos.extraerDatos();
		assertEquals(-1, extractorDatos.getEstadoExtraccionDatos());
		
		System.out.println("////////////////////////////////////////////////////////////////");
	}
}
