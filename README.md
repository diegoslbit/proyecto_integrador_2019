![RA Android Extractor](https://bitbucket.org/diegoslbit/proyecto_integrador/raw/master/src/main/resources/images/logo-menu-desplegable.png "RA Android Extractor")

# PROYECTO INTEGRADOR DE INGENIERÍA EN COMPUTACIÓN
--------------------------------------------------

## RA Android Extractor
---------------------------------------------------------------------------
RA Android Extractor es una aplicación de escritorio para extracción de datos de dispositivos moviles con sistema operativo Android. Para realizar la tarea de adquisición de datos a los dispositivos Android, cuenta con técnicas de adquisición de datos.
Estas técnicas son: _métodos de extracción lógica **ADB** y **MTP**_ y _métodos de extracción física **Dirty COW**, **Recovery** y **LG LAF**_.
Es importante destacar que los métodos que implementa RA Android Extractor, son considerados forense sólidos, ya que mantienen la integridad de la evidencia digital del dispositivo.

Por otro lado, la aplicación tiene soporte para los fabricantes **LG**, **Samsung** y **Motorola**, ya que son los más comercializados a nivel regional y mundial.

#### PLATAFORMA SOPORTADA
* Linux

#### ¿COMO FUNCIONA RA ANDROID EXTRACTOR?
* RA Android Extractor solicita que se conecte un dispositivo móvil Android al ordenador para efectuar una extracción de datos. 
* Luego, se debe elegir si se quiere efectuar una detección automática o manual del dispositivo Android.
* Si se elige realizar una detección automática, se efectúa un reconocimiento automático del dispositivo conectado al ordenador siempre y cuando se habilite el modo de depuración en el dispositivo.
* En caso que se elija la detección manual, se debe elegir las características del dispositivo al que se le quiere realizar la extracción de datos.
* Se debe seleccionar el directorio donde se va alojar la extracción de datos. La unidad de almacenamiento puede ser una tarjeta SD, un pendrive o el mismo disco duro del ordenador.
* Se debe seleccionar un método de extracción de datos, ya sea físico o lógico.
* Por último, se visualiza el proceso de extracción de datos.

#### DESCARGA
[RA-Android-Extractor-v1.0.0.zip](https://bitbucket.org/diegoslbit/proyecto_integrador_2019/raw/372b783969735b9f6540672669ed254df842ef15/release/RA-Android-Extractor.zip)

#### INSTALACIÓN Y EJECUCIÓN
##### 1. Descomprimir archivo RA-Android-Extractor.zip
`~$ unzip RA-Android-Extractor.zip`

##### 2. Dirigirse a directorio RA-Android-Extractor
`~$ cd RA-Android-Extractor/`

##### 3. Ejecutar RA Android Extractor (las dependencias se instalan automáticamente si es necesario).
`~$ bash RA-Android-Extractor.sh`

#### CREAR ACCESO DIRECTO
##### 1. Crear archivo .desktop
`~$ sudo gedit /usr/share/applications/RA-Android-Extractor.desktop`

##### 2. Editar archivo .desktop
```
[Desktop Entry]
Name=RA Android Extractor
Comment=Herramienta de extracción de datos
Exec=bash /home/usuario/carpetaPrograma/RA-Android-Extractor.sh /home/usuario/carpetaPrograma/
Icon=/home/usuario/carpetaPrograma/icono.png
Terminal=false
Type=Application
```

##### 3. Ejecutar acceso directo RA Android Extractor
* Buscar acceso directo RA Android Extractor en Dash del sistema operativo ubuntu.
* Arrastrar accseso directo RA Android Extractor a Lanzador.
* Pulsar sobre acceso directo RA Android Extractor.

#### AUTORES
Apellido y Nombre                 | DNI        | Correo electrónico      | Número de teléfono
--------------------------------- | -----------| ------------------------| ------------------
SOSA LUDUEÑA, Diego Matías        | 38.182.276 | diegosl1294@gmail.com   | 0351-153956000
MONTIEL, Emiliano Ariel           | 31.458.224 | emilianodemo@gmail.com  | 0351-156177710
CHOQUEVILCA ZOTAR, Gustavo Braian | 37.617.402 | g.choquevilca@gmail.com | 0351-155217500