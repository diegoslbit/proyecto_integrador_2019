package test.java;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.junit.Before;
import org.junit.Test;

import main.java.model.DetectarManual;
import main.java.model.Dispositivo;
import main.java.model.ExtractorDatos;
import main.java.model.ExtraerPorMtp;
import main.java.model.Hash;
import main.java.model.ReconocedorDispositivo;
import main.java.model.Reporte;
import main.java.model.UnidadAlmacenamiento;

public class TestGeneracionReporte 
{
	private static List<Dispositivo> listaDispositivos;
	private ReconocedorDispositivo reconocedorDispositivo;
	private UnidadAlmacenamiento unidadAlmacenamiento;
	private ExtractorDatos extractorDatos;
	private Scanner entrada;
	private Reporte reporte;
	private Hash hash;
	
	@Before
	public void init()
	{
		listaDispositivos = new ArrayList<Dispositivo>();
		listaDispositivos.add(new Dispositivo("samsung", "GT-I8550L", "4.1.2", "JZO54K.I8550LUBUANF1"));
		listaDispositivos.add(new Dispositivo("samsung", "GT-I8260L", "4.1.2", "JZO54K.I8260LUBAMG3"));
		listaDispositivos.add(new Dispositivo("LGE", "LG-D625", "4.4.2", "KOT49I"));
		listaDispositivos.add(new Dispositivo("LGE", "Nexus 4", "4.2.2", "JDQ39"));
		listaDispositivos.add(new Dispositivo("LGE", "LG-H440AR", "5.0.1", "LRX21Y"));
		listaDispositivos.add(new Dispositivo("LGE", "LG-M250", "7.0", "NRD90U"));
		listaDispositivos.add(new Dispositivo("motorola", "XT890", "4.1.2", "9.8.2I-50_SML-23"));
	}
	
	@Test
	public void testGeneracionReporte() 
	{
		System.out.println("////////////////////////TEST GENERAR REPORTE////////////////////");
		entrada = new Scanner(System.in);
		
		System.out.print("Conectar dispositivo samsung, GT-I8550L, 4.1.2, JZO54K.I8550LUBUANF1 a ordenador. Pulsar ENTER para avanzar: ");
		entrada.nextLine();
		reconocedorDispositivo = new DetectarManual(listaDispositivos, "samsung, GT-I8550L, 4.1.2, JZO54K.I8550LUBUANF1");
		reconocedorDispositivo.detectarDispositivo();
		
		System.out.print("Introducir path directorio de unidad de almacenamiento: ");		
		String rutaDestino = entrada.nextLine(); // /home/diego/Documentos/Extraccion
		unidadAlmacenamiento = new UnidadAlmacenamiento();
		unidadAlmacenamiento.guardarRutaDestino(rutaDestino); 
		
		extractorDatos = new ExtraerPorMtp(reconocedorDispositivo.getDispositivoDetectado(), unidadAlmacenamiento, true, true);
		
		System.out.print("Habilitar modo Mtp a dispositivo. Pulsar ENTER para avanzar: ");		
		entrada.nextLine();
		
		for(int i=0; i<4; i++)
		{
			extractorDatos.extraerDatos();
		}
		
		hash = new Hash(unidadAlmacenamiento, true, true);
		hash.generarHash(extractorDatos.getFormatoRuta());
		reporte = new Reporte(reconocedorDispositivo.getDispositivoDetectado(), unidadAlmacenamiento, hash, extractorDatos);
		assertEquals(true, reporte.generarReporte());
		
		System.out.println("////////////////////////////////////////////////////////////////");
	}

}
