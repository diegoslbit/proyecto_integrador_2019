package test.java;

import static org.junit.Assert.assertEquals;

import java.util.Scanner;

import org.junit.Before;
import org.junit.Test;

import main.java.model.UnidadAlmacenamiento;

public class TestSeleccionLugarAlmacenamiento 
{
	private UnidadAlmacenamiento unidadAlmacenamiento;
	private Scanner entrada;
	
	@Before
	public void init()
	{
		unidadAlmacenamiento = new UnidadAlmacenamiento();
	}
	
	@Test
	public void testSeleccionarRutaDestino() 
	{
		System.out.println("//////////////////TEST SELECCIONAR RUTA DESTINO/////////////////");
		entrada = new Scanner(System.in);
		
		
		System.out.print("Introducir path directorio de unidad de almacenamiento: ");		
		String rutaDestino = entrada.nextLine(); // /home/diego/Documentos/Extraccion
		assertEquals(true, unidadAlmacenamiento.guardarRutaDestino(rutaDestino));
		assertEquals(rutaDestino, unidadAlmacenamiento.getRutaDestino());
		
		System.out.print("Introducir path fichero de unidad de almacenamiento: ");		
		rutaDestino = entrada.nextLine(); // /home/diego/Documentos/Extraccion/archiv0.txt
		assertEquals(false, unidadAlmacenamiento.guardarRutaDestino(rutaDestino));
		assertEquals("", unidadAlmacenamiento.getRutaDestino());
		
		System.out.print("Introducir path vacio de unidad de almacenamiento: ");		
		rutaDestino = entrada.nextLine(); // espacio en blanco
		assertEquals(false, unidadAlmacenamiento.guardarRutaDestino(rutaDestino));
		assertEquals("", unidadAlmacenamiento.getRutaDestino());
		System.out.println("////////////////////////////////////////////////////////////////");
	}
	
	@Test
	public void testTamanioLegiblePorHumano() 
	{
		long tamanioNoLegible = 1;
		assertEquals("1 B", unidadAlmacenamiento.byteCountToDisplaySize(tamanioNoLegible));
		tamanioNoLegible = 1024;
		assertEquals("1 KB", unidadAlmacenamiento.byteCountToDisplaySize(tamanioNoLegible));
		tamanioNoLegible = 1024*1024;
		assertEquals("1 MB", unidadAlmacenamiento.byteCountToDisplaySize(tamanioNoLegible));
		tamanioNoLegible = 1024*1024*1024;
		assertEquals("1 GB", unidadAlmacenamiento.byteCountToDisplaySize(tamanioNoLegible));
	}
}
