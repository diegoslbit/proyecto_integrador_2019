package test.java;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import main.java.model.DetectarManual;
import main.java.model.Dispositivo;
import main.java.model.ReconocedorDispositivo;

public class TestDeteccionDispositivoManual 
{
	private static List<Dispositivo> listaDispositivos;
	private ReconocedorDispositivo reconocedorDispositivo;
	
	@Before
	public void init()
	{
		listaDispositivos = new ArrayList<Dispositivo>();
		listaDispositivos.add(new Dispositivo("samsung", "GT-I8550L", "4.1.2", "JZO54K.I8550LUBUANF1"));
		listaDispositivos.add(new Dispositivo("samsung", "GT-I8260L", "4.1.2", "JZO54K.I8260LUBAMG3"));
		listaDispositivos.add(new Dispositivo("LGE", "LG-D625", "4.4.2", "KOT49I"));
		listaDispositivos.add(new Dispositivo("LGE", "Nexus 4", "4.2.2", "JDQ39"));
		listaDispositivos.add(new Dispositivo("LGE", "LG-H440AR", "5.0.1", "LRX21Y"));
		listaDispositivos.add(new Dispositivo("LGE", "LG-M250", "7.0", "NRD90U"));
		listaDispositivos.add(new Dispositivo("motorola", "XT890", "4.1.2", "9.8.2I-50_SML-23"));
	}
	
	@Test
	public void testDetectarDispositivoManualmente() 
	{
		System.out.println("/////////////////////TEST DETECTAR MANUALMENTE//////////////////");
		System.out.println("Se selecciono samsung, GT-I8550L, 4.1.2, JZO54K.I8550LUBUANF1");
		reconocedorDispositivo = new DetectarManual(listaDispositivos, "samsung, GT-I8550L, 4.1.2, JZO54K.I8550LUBUANF1");
		assertEquals(true, reconocedorDispositivo.detectarDispositivo());
		
		System.out.println("Se selecciono LGE, LG-H440AR, 5.0.1, LRX21Y");
		reconocedorDispositivo = new DetectarManual(listaDispositivos, "LGE, LG-H440AR, 5.0.1, LRX21Y");
		assertEquals(true, reconocedorDispositivo.detectarDispositivo());
		
		System.out.println("Se selecciono XT890, 4.1.2, 9.8.2I-50_SML-23");
		reconocedorDispositivo = new DetectarManual(listaDispositivos, "motorola, XT890, 4.1.2, 9.8.2I-50_SML-23");
		assertEquals(true, reconocedorDispositivo.detectarDispositivo());
		
		System.out.println("////////////////////////////////////////////////////////////////");
	}
}
