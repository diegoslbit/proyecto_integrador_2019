package main.java.model;

/**
 * Esta clase, contiene propiedades asociadas 
 * de un conjunto de dispositivos Android soportados.  
 * @author Sosa Ludueña Diego
 * @author Choquevilca Gustavo
 * @author Montiel Emiliano
 *
 */
public class Dispositivo
{
	private String fabricante;
	private String modelo;
	private String versionSo;
	private String numeroCompilacion;
	private String idFabricante;
	
	private String rutaParticion;
	private String rutaRecoveryFabrica;
	private String rutaRecoveryPersonalizado;
	private String rutaDirtyCowDcow;
	private String rutaDirtyCowRunas;
	private String rutaLglaf;
	private String rutaLglafPartitions;
	private String rutaLglafExtractPartitions;
	private String rutaMtp;
	private String rutaSdcard0;
	private String rutaSdcard1;
	private String cantidadBloques;
	
	private long tamanioParticion;
	private long tamanioLogico;
	
	private boolean seleccionado;
	private boolean metodoDirtyCow;
	private boolean metodoRecovery;
	private boolean metodoLgLaf;
	private boolean metodoMtp;
	private boolean metodoAdb;
	
	/**
	 * Este constructor, inicializa atributos dependiendo del tipo de dispositivo Android.
	 * @param fabricante Variable String que contiene marca de dispositivo Android.
	 * @param modelo Variable String que contiene modelo de dispositivo Android.
	 * @param versionSo Variable String que contiene versión de sistema operativo Android.
	 * @param numeroCompilacion Variable String que contiene número de compilación de dispositivo Android.
	 */
	public Dispositivo(String fabricante, String modelo, String versionSo, String numeroCompilacion)
	{
		this.fabricante = fabricante;
		this.modelo = modelo;
		this.versionSo = versionSo;
		this.numeroCompilacion = numeroCompilacion;
		this.seleccionado = false;
		
		switch(fabricante)
		{
			case "samsung":
			{
				idFabricante = "1256";
				rutaMtp = "";
				rutaLglaf = "";
				rutaLglafExtractPartitions = "";
				rutaLglafPartitions = "";
				cantidadBloques = "";
				tamanioLogico = 0;
				
				switch(numeroCompilacion)
				{
					case "JZO54K.I8550LUBUANF1":
					{
						metodoDirtyCow = true;
						metodoRecovery = true;
						metodoLgLaf = false;
						metodoMtp = true;
						metodoAdb = true;
						
						rutaParticion = "/dev/block/mmcblk0p24";
						//rutaParticion = "/dev/block/mmcblk0p8";
						rutaRecoveryFabrica = "main/resources/devices/recovery/samsung/win/recoveryFabrica.img";
						rutaRecoveryPersonalizado = "main/resources/devices/recovery/samsung/win/recoveryPersonalizado.img";
						rutaDirtyCowDcow = "main/resources/devices/dirtycow/samsung/win/dirtycow";				
						rutaDirtyCowRunas = "main/resources/devices/dirtycow/samsung/win/run-as";
						rutaSdcard0 = "/storage/sdcard0";
						rutaSdcard1 = "/storage/extSdCard";
						tamanioParticion = 5573804032L; //5443168 blocks × 1024 bytes
						//tamanioParticion = 12582912L;
						break;
					}
					case "JZO54K.I8260LUBAMG3":
					{
						metodoDirtyCow = true;
						metodoRecovery = true;
						metodoLgLaf = false;
						metodoMtp = true;
						metodoAdb = true;
						
						rutaParticion = "/dev/block/mmcblk0p24";
						rutaRecoveryFabrica = "main/resources/devices/recovery/samsung/core/recoveryFabrica.img";
						rutaRecoveryPersonalizado = "main/resources/devices/recovery/samsung/core/recoveryPersonalizado.img";
						rutaDirtyCowDcow = "main/resources/devices/dirtycow/samsung/core/dirtycow";
						rutaDirtyCowRunas = "main/resources/devices/dirtycow/samsung/core/run-as";
						rutaSdcard0 = "/storage/sdcard0";
						rutaSdcard1 = "/storage/extSdCard";
						tamanioParticion = 5238259712L;
						break;
					}
					case "NRD90M.G570MUBU2BRA3":
					{
						metodoDirtyCow = false;
						metodoRecovery = true;
						metodoLgLaf = false;
						metodoMtp = true;
						metodoAdb = true;
						
						rutaParticion = "/dev/block/mmcblk0p24";
						rutaRecoveryFabrica = "main/resources/devices/recovery/samsung/j5prime/recoveryFabrica.img";
						rutaRecoveryPersonalizado = "main/resources/devices/recovery/samsung/j5prime/recoveryPersonalizado.img";
						rutaDirtyCowDcow = "";				
						rutaDirtyCowRunas = "";
						rutaSdcard0 = "/storage/self/primary";
						rutaSdcard1 = "/storage";
						tamanioParticion = 12075401216L;
						break;
					}
				}
				break;
			}
			case "LGE":
			{
				rutaRecoveryFabrica = "";
				rutaRecoveryPersonalizado = "";
				rutaLglaf = "main/resources/devices/lglaf/lglaf.py";
				rutaLglafPartitions = "main/resources/devices/lglaf/partitions.py";
				rutaLglafExtractPartitions = "main/resources/devices/lglaf/extract-partitions.py";
				rutaMtp = "";
				tamanioLogico = 0;
				
				switch(numeroCompilacion)
				{
					case "JDQ39":
					{
						metodoDirtyCow = true;
						metodoRecovery = false;
						metodoLgLaf = false;
						metodoMtp = true;
						metodoAdb = true;
						
						idFabricante = "6353";
						rutaParticion = "/dev/block/mmcblk0p24";
						rutaDirtyCowDcow = "";
						rutaDirtyCowRunas = "";
						rutaSdcard0 = "/storage/sdcard0";
						rutaSdcard1 = "/storage/sdcard1";
						cantidadBloques = "";
						tamanioParticion = 0;
						break;
					}
					case "NRD90U":
					{
						metodoDirtyCow = false;
						metodoRecovery = false;
						metodoLgLaf = true;
						metodoMtp = true;
						metodoAdb = true;
						
						idFabricante = "4100";
						rutaParticion = "/dev/block/mmcblk0";
						rutaDirtyCowDcow = "";
						rutaDirtyCowRunas = "";
						rutaSdcard0 = "/storage/sdcard0";
						rutaSdcard1 = "/storage/sdcard1";
						cantidadBloques = "15392768";
						tamanioParticion = 15762194432L;//TODAS PARTICIONES
						break;
					}
					case "LRX21Y":
					{
						metodoDirtyCow = false;
						metodoRecovery = false;
						metodoLgLaf = true;
						metodoMtp = true;
						metodoAdb = true;
						
						idFabricante = "4100";
						rutaParticion = "/dev/block/mmcblk0";
						rutaDirtyCowDcow = "";
						rutaDirtyCowRunas = "";
						rutaSdcard0 = "/storage/sdcard0";
						rutaSdcard1 = "/storage/external_SD";
						cantidadBloques = "7634944";
						tamanioParticion = 7818182656L;//TODAS PARTICIONES
						break;
					}
					case "KOT49I":
					{
						metodoDirtyCow = true;
						metodoRecovery = false;
						metodoLgLaf = true;
						metodoMtp = true;
						metodoAdb = true;
						
						idFabricante = "4100";
						rutaParticion = "/dev/block/mmcblk0p20";
						rutaDirtyCowDcow = "main/resources/devices/dirtycow/lge/lg2mini/dirtycow";
						rutaDirtyCowRunas = "main/resources/devices/dirtycow/lge/lg2mini/run-as";
						rutaSdcard0 = "/storage/sdcard0";
						rutaSdcard1 = "/storage/external_SD";
						tamanioParticion = 4812963840L;//USERDATA
						cantidadBloques = "4700160";
						break;
					}
				}
				break;
			}
			case "motorola":
			{
				metodoDirtyCow = true;
				metodoRecovery = false;
				metodoLgLaf = false;
				metodoMtp = true;
				metodoAdb = true;
				
				//idFabricante = "4046"; //Sony
				idFabricante = "8888";
				rutaParticion = "/dev/block/mmcblk0p17";
				rutaDirtyCowDcow = "main/resources/devices/dirtycow/motorola/XT890/dirtycow";
				rutaDirtyCowRunas = "main/resources/devices/dirtycow/motorola/XT890/run-as";
				rutaRecoveryFabrica = "";
				rutaRecoveryPersonalizado = "";
				rutaLglaf = "";
				rutaLglafPartitions = "";
				rutaLglafExtractPartitions = "";
				rutaMtp = "";
				rutaSdcard0 = "/storage/sdcard0";
				rutaSdcard1 = "/storage/sdcard1";
				cantidadBloques = "";
				tamanioParticion = 5599640576L;
				tamanioLogico = 0;
				break;
			}
			default:
			{
				metodoDirtyCow = false;
				metodoRecovery = false;
				metodoLgLaf = false;
				metodoAdb = false;
				metodoMtp = false;
				
				idFabricante = "";
				rutaParticion = "";
				rutaDirtyCowDcow = "";
				rutaDirtyCowRunas = "";
				rutaRecoveryFabrica = "";
				rutaRecoveryPersonalizado = "";
				rutaLglaf = "";
				rutaLglafPartitions = "";
				rutaLglafExtractPartitions = "";
				rutaMtp = "";
				rutaSdcard0 = "";
				rutaSdcard1 = "";
				cantidadBloques = "";
				tamanioParticion = 0;
				tamanioLogico = 0;
			}
		}
	}
	
	/**
	 * Esta función, retorna fabricante de instancia dispositivo Android.
	 * @return Retorna variable String llamada fabricante que contiene
	 * nombre de fabricante de instancia dispositivo Android.
	 */
	public String getFabricante()
	{
		return fabricante;
	}
	
	/**
	 * Esta función, retorna modelo de instancia dispositivo Android.
	 * @return Retorna una variable String llamada modelo que contiene
	 * nombre de modelo de instancia dispositivo Android.
	 */
	public String getModelo()
	{
		return modelo;
	}
	
	/**
	 * Esta función, retorna versión de sistema operativo de instancia dispositivo Android.
	 * @return Retorna variable String llamada versionSo que contiene
	 * versión de sistema operativo de instancia dispositivo Android.
	 */
	public String getVersionSo()
	{
		return versionSo;
	}
	
	/**
	 * Esta funcion, retorna numero de compilacion de instancia dispositivo Android.
	 * @return Retorna variable String llamada numeroCompilacion que contiene
	 * numero de compilacion de instancia dispositivo Android.
	 */
	public String getNumeroCompilacion()
	{
		return numeroCompilacion;
	}
	
	/**
	 * Esta función, retorna id vendor de instancia dispositivo Android.
	 * @return Retorna variable String llamada idFabricante que contiene
	 * identificador de fabricante de instancia dispositivo Android.
	 */
	public String getIdFabricante()
	{
		return idFabricante;
	}
	
	/**
	 * Esta función, retorna booleano de instancia dispositivo Android.
	 * Si es true indica que instancia Dispositivo se seleccionó.
	 * Si es false indica que instancia Dispositivo no se seleccionó.
	 * @return Retorna variable boolean llamada seleccionado
	 * que indica si se seleccionó o no dispositivo Android.
	 */
	public boolean getSeleccionado()
	{
		return seleccionado;
	}
	
	/**
	 * Esta función, proporciona tamaño de partición de instancia dispositivo Android.
	 * @return Retorna variable long llamada tamanioParticion que contiene
	 * tamaño de partición de instancia dispositivo Android.
	 */
	public long getTamanioParticion()
	{
		return tamanioParticion;
	}
	
	/**
	 * Esta función, retorna booleano de instancia dispositivo Android.
	 * Si es true indica que soporta método Dirty Cow, instancia creada.
	 * Si es false indica que no soporta método Dirty Cow, instancia creada.
	 * @return Retorna variable boolean llamada metodoDirtyCow
	 * que indica si soporta o no soporta método Dirty Cow.
	 */
	public boolean getMetodoDityCow()
	{
		return metodoDirtyCow;
	}
	
	/**
	 * Esta función, retorna booleano de instancia dispositivo Android.
	 * Si es true indica que soporta método Recovery, instancia creada.
	 * Si es false indica que no soporta método Recovery, instancia creada.
	 * @return Retorna variable boolean llamada metodoRecovery
	 * que indica si soporta o no soporta método Recovery.
	 */
	public boolean getMetodoRecovery()
	{
		return metodoRecovery;
	}
	
	/**
	 * Esta función, retorna booleano de instancia dispositivo Android.
	 * Si es true indica que soporta método Lg laf, instancia creada.
	 * Si es false indica que no soporta método Lg laf, instancia creada.
	 * @return Retorna variable boolean llamada metodoLgLaf
	 * que indica si soporta o no soporta método Lg laf.
	 */
	public boolean getMetodoLgLaf()
	{
		return metodoLgLaf;
	}
	
	/**
	 * Esta función, retorna booleano de instancia dispositivo Android.
	 * Si es true indica que soporta método Adb, instancia creada.
	 * Si es false indica que no soporta método Adb, instancia creada.
	 * @return Retorna variable boolean llamada metodoAdb
	 * que indica si soporta o no soporta método Adb.
	 */
	public boolean getMetodoAdb()
	{
		return metodoAdb;
	}
	
	/**
	 * Esta función, retorna un booleano de instancia dispositivo Android.
	 * Si es true indica que soporta método Mtp, instancia creada.
	 * Si es false indica que no soporta método Mtp, instancia creada.
	 * @return Retorna una variable boolean llamada metodoMtp
	 * que indica si soporta o no soporta método Mtp.
	 */
	public boolean getMetodoMtp()
	{
		return metodoMtp;
	}
	
	/**
	 * Esta función, retorna ruta de almacenamiento interno asociada a dispositivo Android conectado.
	 * @return Retorna variable String llamada rutaSdcard0
	 * que contiene ruta de almacenamiento interno asociada a dispositivo Android conectado.
	 */
	public String getRutaSdcard0()
	{
		return rutaSdcard0;
	}
	
	/**
	 * Esta función, retorna ruta de almacenamiento externo asociada a dispositivo Android conectado.
	 * @return Retorna variable String llamada rutaSdcard1
	 * que contiene ruta de almacenamiento externo asociada a dispositivo Android conectado.
	 */
	public String getRutaSdcard1()
	{
		return rutaSdcard1;
	}
	
	/**
	 * Esta función, retorna ruta donde se aloja partición de dispositivo Android.
	 * @return Retorna variable String llamada rutaParticion
	 * que contiene ruta donde se aloja partición de dispositivo Android.
	 */
	public String getRutaParticion()
	{
		return rutaParticion;
	}
	
	/**
	 * Esta función, retorna ruta donde se aloja recovery de fábrica de dispositivo Android.
	 * @return Retorna variable String llamada rutaRecoveryFabrica
	 * que contiene ruta donde se aloja el recovery de fábrica de dispositivo Android.
	 */
	public String getRutaRecoveryFabrica()
	{
		return rutaRecoveryFabrica;
	}
	
	/**
	 * Esta función, retorna ruta donde se aloja recovery personalizado de dispositivo Android.
	 * @return Retorna variable String llamada rutaRecoveryPersonalizado
	 * que contiene ruta donde se aloja recovery de personalizado de dispositivo Android.
	 */
	public String getRutaRecoveryPersonalizado()
	{
		return rutaRecoveryPersonalizado;
	}
	
	/**
	 * Esta función, retorna ruta donde se aloja archivo dirtycow asociado a dispositivo Android conectado.
	 * @return Retorna variable String llamada rutaDirtyCowDcow
	 * que contiene ruta donde se aloja archivo dirtycow asociado a dispositivo Android conectado.
	 */
	public String getRutaDirtyCowDcow()
	{
		return rutaDirtyCowDcow;
	}

	/**
	 * Esta función, retorna ruta donde se aloja archivo run-as asociado a dispositivo Android conectado.
	 * @return Retorna variable String llamada rutaDirtyCowRunas
	 * que contiene ruta donde se aloja archivo run-as asociado a dispositivo Android conectado.
	 */
	public String getRutaDirtyCowRunas()
	{
		return rutaDirtyCowRunas;
	}
	
	/**
	 * Esta función, retorna ruta mtp asociada a dispositivo Android conectado.
	 * @return Retorna variable String llamada rutaMtp
	 * que contiene ruta mtp asociada a dispositivo Android conectado.
	 */
	public String getRutaMtp()
	{
		return rutaMtp;
	}
	
	/**
	 * Esta función, proporciona tamaño de almacenamiento tanto interno como externo de instancia dispositivo Android.
	 * @return Retorna variable long llamada tamanioLogico que contiene
	 * tamaño de directorio de almacenamiento interno y externo de instancia dispositivo Android.
	 */
	public long getTamanioLogico()
	{
		return tamanioLogico;
	}
	
	/**
	 * Esta función, retorna ruta de archivo lglaf.py asociada a dispositivo Android conectado.
	 * @return Retorna variable String llamada rutaLglaf
	 * que contiene ruta de archivo lglaf.py asociada a dispositivo Android conectado.
	 */
	public String getRutaLglaf()
	{
		return rutaLglaf;
	}
	
	/**
	 * Esta función, retorna ruta de archivo partitions.py asociada a dispositivo Android conectado.
	 * @return Retorna variable String llamada rutaLglafPartitions
	 * que contiene ruta de archivo partitions.py asociada a dispositivo Android conectado.
	 */
	public String getRutaLglafPartitions()
	{
		return rutaLglafPartitions;
	}
	
	/**
	 * Esta función, retorna ruta de archivo extract-partitions.py asociada a dispositivo Android conectado.
	 * @return Retorna variable String llamada rutaLglafExtractPartitions
	 * que contiene ruta de archivo extract-partitions.py asociada a dispositivo Android conectado.
	 */
	public String getRutaLglafExtractPartitions()
	{
		return rutaLglafExtractPartitions;
	}
	
	/**
	 * Esta función, retorna cantidad de bloques asociada a particion de dispositivo Android.
	 * @return Retorna variable String llamada cantidadBloques
	 * que contiene cantidad de bloques asociada a partición de dispositivo Android.
	 */
	public String getCantidadBloques()
	{
		return cantidadBloques;
	}
	
	/**
	 *Este método, permite setear en true o false variable booleana seleccionado.
	 * @param seleccionado Variable booleana que tiene cada instancia Dispositivo.
	 * Indica si es seleccionado (true) o no seleccionado (false).
	 */
	public void setSeleccionar(boolean seleccionado)
	{
		this.seleccionado = seleccionado; 
	}
	
	/**
	 *Este metodo, permite setear variable String rutaMtp.
	 * @param rutaMtp Variable String que tiene cada instancia Dispositivo.
	 * Indica ruta mtp asociada a dispositivo Android conectado.
	 */
	public void setRutaMtp(String numeroBus, String numeroDispositivo)
	{
		this.rutaMtp = "/run/user/1000/gvfs/mtp:host=%5Busb%3A" + numeroBus + "%2C" + numeroDispositivo + "%5D/";
	}
	
	/**
	 *Este método, permite setear variable long tamanioLogico.
	 * @param tamanioLogico Variable long que tiene cada instancia Dispositivo.
	 * Indica tamaño de almacenamiento de dispositivo Android conectado.
	 */
	public void setTamanioLogico(long tamanioLogico)
	{
		this.tamanioLogico = tamanioLogico;
	}
}
