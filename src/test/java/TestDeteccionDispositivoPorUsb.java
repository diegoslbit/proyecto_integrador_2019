package test.java;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.junit.Before;
import org.junit.Test;

import main.java.model.DetectarPorUsb;
import main.java.model.Dispositivo;
import main.java.model.ReconocedorDispositivo;

public class TestDeteccionDispositivoPorUsb 
{
	private static List<Dispositivo> listaDispositivos;
	private ReconocedorDispositivo reconocedorDispositivo;
	private Scanner entrada;
	
	@Before
	public void init()
	{
		listaDispositivos = new ArrayList<Dispositivo>();
		listaDispositivos.add(new Dispositivo("samsung", "GT-I8550L", "4.1.2", "JZO54K.I8550LUBUANF1"));
		listaDispositivos.add(new Dispositivo("samsung", "GT-I8260L", "4.1.2", "JZO54K.I8260LUBAMG3"));
		listaDispositivos.add(new Dispositivo("LGE", "LG-D625", "4.4.2", "KOT49I"));
		listaDispositivos.add(new Dispositivo("LGE", "Nexus 4", "4.2.2", "JDQ39"));
		listaDispositivos.add(new Dispositivo("LGE", "LG-H440AR", "5.0.1", "LRX21Y"));
		listaDispositivos.add(new Dispositivo("LGE", "LG-M250", "7.0", "NRD90U"));
		listaDispositivos.add(new Dispositivo("motorola", "XT890", "4.1.2", "9.8.2I-50_SML-23"));
	}
	
	@Test
	public void testDetectarDispositivoPorUsb() 
	{
		System.out.println("///////////////TEST DETECTAR DISPOSITIVO POR USB////////////////");
		entrada = new Scanner(System.in);
		
		System.out.print("Conectar dispositivo Samsung a ordenador. Pulsar ENTER para avanzar: ");		
		entrada.nextLine();
		reconocedorDispositivo = new DetectarPorUsb(listaDispositivos);
		assertEquals(true, reconocedorDispositivo.detectarDispositivo());
		
		System.out.print("Conectar dispositivo LGE a ordenador. Pulsar ENTER para avanzar: ");		
		entrada.nextLine();
		reconocedorDispositivo = new DetectarPorUsb(listaDispositivos);
		assertEquals(true, reconocedorDispositivo.detectarDispositivo());
		
		System.out.print("Conectar dispositivo Motorola a ordenador. Pulsar ENTER para avanzar: ");		
		entrada.nextLine();
		reconocedorDispositivo = new DetectarPorUsb(listaDispositivos);
		assertEquals(true, reconocedorDispositivo.detectarDispositivo());
		
		System.out.print("Conectar dispositivo que no sea Samsung ni LGE ni Motorola a ordenador. Pulsar ENTER para avanzar: ");		
		entrada.nextLine();
		reconocedorDispositivo = new DetectarPorUsb(listaDispositivos);
		assertEquals(false, reconocedorDispositivo.detectarDispositivo());
		
		System.out.print("No conectar dispositivo a ordenador. Pulsar ENTER para avanzar: ");		
		entrada.nextLine();
		reconocedorDispositivo = new DetectarPorUsb(listaDispositivos);
		assertEquals(false, reconocedorDispositivo.detectarDispositivo());
		
		System.out.println("////////////////////////////////////////////////////////////////");
	}
}
